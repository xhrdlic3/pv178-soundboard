# Assignment #1

```
• Vytvořte slovní popis systému, který budete řešit v rámci
semestrálního projektu. Popište jednotlivé uživatelské role
a funkce systému.

• Uveďte výčet technologií, které hodláte pro realizaci
projektu použít. Pro každou technologii uveďte, jak ji
ovládáte (absolvované kurzy, dosavadní zkušenosti).

• Rozhodněte, jaký model životního cyklu použijete. Toto
rozhodnutí zdůvodněte.
```

## Text summary

Name: **Soundboard**

Description: *Desktop app for playing preselected sound bites.*

Features:
  - sounds playback from a list
  - sound selection from local files or YT videos
  - key mapping of playback to keyboard shortcuts

Roles:
  - **Arbiter**
    - sets everything up
  - **Player**
    - uses the app to play sounds


## Technologies

 - **.NET 6.0 & C#**
   - knowledge in the scope of the PV178 course
 - **Avalonia**
   - it's a modern cross-platform UI
   - needed as an alternative to WindowsForms, as development will take place on Linux
   - no previous knowledge, only familiar with Java's Swing framework for desktop UI's
 - **SQLLite**
   - it's a DB engine allowing for file-based DB, which is needed for a Desktop application
   - previous personal experience with PostgreSQL from other courses should suffice
 - **Git VCS**
   - 3 years of continuous ~~pain~~ experience

## Life-cycle

Model: **Agile**

Phases:
  1) *pre-game*: specifications in the scope of this PB175 course
  2) *game*: development
  3) *post-game*: release

Why:
Easily adapts to changes of direction mid-development.