# Assignment #2, part 2

```
• Vytvořte návrhy obrazovek aplikace.
• Vytvořte návrh datového modelu.
• Vytvořte matici obrazovek.
```

## Screens

### Screen #1, MainWindow

![MainWindow!](MainWindow.png "MainWindow")
<div style="page-break-after: always"></div>

### Screen #2, Add Local Sound

![AddLocal!](AddLocal.png "AddLocal")
<div style="page-break-after: always"></div>

### Screen #3, Add YT Sound

![AddYT!](AddYT.png "AddYT")
<div style="page-break-after: always"></div>

### Screen #4, Edit Sound

![Edit!](Edit.png "Edit")
<div style="page-break-after: always"></div>

### Screen #5, Delete Sound

![Delete!](Delete.png "Delete")
<div style="page-break-after: always"></div>

---
## Datamodel

![ERD!](erd.png "Soundboard ERD")

---
## Screen matrix

| Entity.prop \ Use | #1 Main Window | #2 Add Local Sound | #3 Add YT Sound | #4 Edit Sound | #5 Delete Sound |
|-------------------|----------------|--------------------|-----------------|---------------|-----------------|
| Sound             |                | C                  | C               |               | R               |
| Sound.name        | D              |                    |                 | M             |                 |
| Sound.path        | D              | E                  | E               | D M           |                 |
| Sound.keyBind     | D              |                    |                 | D M E         |                 |
| Sound.isFavourite | D M            |                    |                 |               |                 |

where `D=displayed, E=entered, M=modified, C=created, R=removed`.
