# Soundboard installation guide

## Required technologies to run
- `dotnet`
  - version 6.0
- `sqlite`
  - used for local database
- `ffmpeg`
  - used for file format conversions when downloading sounds from YouTube
- `mpg123`
  - used for playing .mp3 files
- `dotnet ef`
  - used for applying migrations
  - installed with `dotnet tool install --global dotnet-ef`

Everything else is provided in-app with NuGet packages, requiring no further manual installs.

## How to run

```shell
cd PV178Soundboard        # change to project directory if not already in it
dotnet ef database update # create DB and apply migrations
dotnet build              # build the app                
dotnet run                # run the app                
```