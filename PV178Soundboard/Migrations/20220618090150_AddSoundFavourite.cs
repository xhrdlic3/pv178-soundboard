﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PV178Soundboard.Migrations
{
    public partial class AddSoundFavourite : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsFavourite",
                table: "Sounds",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsFavourite",
                table: "Sounds");
        }
    }
}
