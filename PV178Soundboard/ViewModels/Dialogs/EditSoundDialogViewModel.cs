using System;
using System.Reactive;
using System.Windows.Input;
using Avalonia.Input;
using PV178Soundboard.Models;
using ReactiveUI;

namespace PV178Soundboard.ViewModels.Dialogs;

public class EditSoundDialogViewModel : ViewModelBase
{
    private readonly Sound _sound;

    public EditSoundDialogViewModel(Sound backingSound)
    {
        _sound = backingSound;

        ShowBrowseDialog = new Interaction<string?, string[]?>();

        OkButtonClicked = ReactiveCommand.Create(() => _sound);
        BrowseButtonClicked = ReactiveCommand.Create(() =>
        {
            ShowBrowseDialog
                .Handle(Path)
                .Subscribe(newPaths =>
                {
                    if (newPaths == null || newPaths.Length == 0)
                    {
                        return;
                    }

                    Path = newPaths[0];
                    this.RaisePropertyChanged(nameof(Path));
                });
        });
    }

    public EditSoundDialogViewModel() : this(new Sound("")) { }

    public ICommand BrowseButtonClicked { get; set; }

    public Interaction<string?, string[]?> ShowBrowseDialog { get; }

    public string Path
    {
        get => _sound.Path;
        set
        {
            _sound.Path = value;
            this.RaisePropertyChanged(nameof(Path));
        }
    }

    public ReactiveCommand<Unit, Sound> OkButtonClicked { get; }

    public ReactiveCommand<Unit, Unit> CancelButtonClicked { get; } = ReactiveCommand.Create(() => { });

    public KeyGesture? KeyBind
    {
        set => _sound.KeyBind = value;
        get => _sound.KeyBind;
    }
}