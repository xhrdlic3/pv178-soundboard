﻿using System;
using System.IO;
using System.Reactive;
using System.Threading.Tasks;
using PV178Soundboard.Models;
using ReactiveUI;
using YoutubeExplode;
using YoutubeExplode.Converter;
using YoutubeExplode.Videos;

namespace PV178Soundboard.ViewModels.Dialogs;

public class YouTubeDownloadDialogViewModel : ViewModelBase
{
    private static readonly string DefaultPath =
        Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Downloads");

    private double? _downloadProgress;
    private string _errorText = "";

    private string _pathInput = DefaultPath;

    public YouTubeDownloadDialogViewModel()
    {
        BrowseButtonClicked = ReactiveCommand.Create(() =>
        {
            ShowBrowseDialog.Handle(PathInput).Subscribe(maybeNewPath =>
            {
                if (maybeNewPath != null)
                {
                    PathInput = maybeNewPath;
                }
            });
        });

        DownloadButtonClicked = ReactiveCommand.CreateFromTask(ExecuteDownload);
    }

    public static string PathInputLabel => "Destination path:";

    public static string UrlInputLabel => "YouTube video URL:";

    public static string DownloadButtonText => "Download";

    public static string CancelButtonText => "Cancel";

    public string PathInput
    {
        get => _pathInput;
        set => this.RaiseAndSetIfChanged(ref _pathInput, value);
    }

    public string UrlInput { get; set; } = "";

    public ReactiveCommand<Unit, Sound?> DownloadButtonClicked { get; }

    public ReactiveCommand<Unit, Unit> CancelButtonClicked { get; } = ReactiveCommand.Create(() => { });

    public ReactiveCommand<Unit, Unit> BrowseButtonClicked { get; }

    public Interaction<string, string?> ShowBrowseDialog { get; } = new();

    public string ErrorText
    {
        get => _errorText;
        set => this.RaiseAndSetIfChanged(ref _errorText, value);
    }

    public double? DownloadProgressValue
    {
        get => _downloadProgress;
        set => this.RaiseAndSetIfChanged(ref _downloadProgress, value);
    }

    private async Task<Sound?> ExecuteDownload()
    {
        ErrorText = "";
        var youtube = new YoutubeClient();

        VideoId videoId;
        try
        {
            videoId = VideoId.Parse(UrlInput);
        }
        catch (ArgumentException e)
        {
            ErrorText = e.Message;
            return null;
        }

        PathInput = SanitizePathInput(PathInput);

        try
        {
            await youtube.Videos.DownloadAsync(
                videoId,
                PathInput,
                builder => builder
                    .SetContainer(Path.GetExtension(PathInput)[1..])
                    .SetFFmpegPath("/usr/bin/ffmpeg"), // TODO: cross-platform solution
                new Progress<double>(percentage => DownloadProgressValue = percentage));
        }
        catch (Exception e)
        {
            ErrorText = e.Message;
            return null;
        }

        return new Sound(PathInput);
    }

    private static string SanitizePathInput(string pathInput)
    {
        if (pathInput.Length == 0)
        {
            pathInput = DefaultPath;
        }

        if (Directory.Exists(pathInput))
        {
            return Path.Combine(pathInput, "downloaded.mp3");
        }

        if (string.IsNullOrEmpty(Path.GetExtension(pathInput)))
        {
            return pathInput + ".mp3";
        }

        return pathInput;
    }
}