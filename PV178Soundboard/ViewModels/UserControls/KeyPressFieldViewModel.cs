﻿using System.ComponentModel;
using Avalonia.Input;

namespace PV178Soundboard.ViewModels.UserControls;

public class KeyPressFieldViewModel : INotifyPropertyChanged
{
    public const string NoBindString = "No keybind";

    public string KeyBindString => KeyBind?.ToString() ?? NoBindString;
    public KeyGesture? KeyBind { get; private set; }

    public event PropertyChangedEventHandler? PropertyChanged;

    public void SetKeyBind(KeyGesture? keyGesture)
    {
        KeyBind = keyGesture;
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(KeyBindString)));
    }

    public void SetKeyBind(KeyEventArgs e)
    {
        SetKeyBind(new KeyGesture(e.Key, e.KeyModifiers));
    }

    public void ClearKeyBind()
    {
        KeyBind = null;
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(KeyBindString)));
    }
}