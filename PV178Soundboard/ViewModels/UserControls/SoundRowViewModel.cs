﻿using System;
using System.Reactive;
using System.Threading.Tasks;
using System.Windows.Input;
using Avalonia.Input;
using NetCoreAudio;
using PV178Soundboard.Models;
using ReactiveUI;

namespace PV178Soundboard.ViewModels.UserControls;

public class SoundRowViewModel : ViewModelBase
{
    private const string Pause = "\u25a0";
    private const string Play = "\u25b6";

    private readonly Player _player = new();

    public readonly Interaction<KeyGesture?, Unit> ChangeHotKey;

    private string _playButtonText = Play;

    public SoundRowViewModel() : this(new Sound("")) { }

    public SoundRowViewModel(Sound sound)
    {
        Sound = sound;

        ChangeHotKey = new Interaction<KeyGesture?, Unit>();

        ChangeHotKeyCommand = ReactiveCommand.Create(() => HotKeyCommandBody(Sound.KeyBind));

        ClearHotKeyCommand = ReactiveCommand.Create(() => HotKeyCommandBody(null));
    }

    public string PlayButtonText
    {
        get => _playButtonText;
        set => this.RaiseAndSetIfChanged(ref _playButtonText, value);
    }

    public string Path => Sound.Path;

    public string SoundName => Sound.Name;

    public bool IsFavourite
    {
        get => Sound.IsFavourite;
        set => Sound.IsFavourite = value;
    }

    public bool IsPlaying => _player.Playing;

    public ICommand ChangeHotKeyCommand { get; }

    public ICommand ClearHotKeyCommand { get; }

    public Sound Sound { get; }

    public string KeyBindString => Sound.KeyBind?.ToString() ?? KeyPressFieldViewModel.NoBindString;

    private void HotKeyCommandBody(KeyGesture? interactionInput)
    {
        this.RaisePropertyChanged(nameof(KeyBindString));
        ChangeHotKey
            .Handle(interactionInput)
            .Subscribe(_ => { });
    }

    public async Task PlayButtonClicked()
    {
        if (PlayButtonText == Play)
        {
            PlayButtonText = Pause;

            _player.PlaybackFinished += PlayBackFinishedHandler;

            await _player.Play(Path);
        }
        else
        {
            await Stop();
        }
    }

    public async Task Stop()
    {
        PlayButtonText = Play;
        await _player.Stop();
    }

    private void PlayBackFinishedHandler(object? s, EventArgs e)
    {
        PlayButtonText = Play;
        _player.PlaybackFinished -= PlayBackFinishedHandler;
    }

    private bool Equals(SoundRowViewModel other)
    {
        return Sound.Equals(other.Sound);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return false;
        }

        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        return obj.GetType() == GetType() && Equals((SoundRowViewModel) obj);
    }

    public override int GetHashCode()
    {
        return Sound.GetHashCode();
    }
}