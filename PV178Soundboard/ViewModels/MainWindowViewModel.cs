﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Avalonia.Controls;
using Avalonia.Controls.Selection;
using Avalonia.Threading;
using DynamicData;
using MessageBox.Avalonia;
using MessageBox.Avalonia.BaseWindows.Base;
using MessageBox.Avalonia.DTO;
using MessageBox.Avalonia.Enums;
using PV178Soundboard.Database;
using PV178Soundboard.Models;
using PV178Soundboard.ViewModels.Dialogs;
using PV178Soundboard.ViewModels.UserControls;
using ReactiveUI;

namespace PV178Soundboard.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    private readonly ReadOnlyObservableCollection<SoundRowViewModel> _visibleSounds;
    private bool _allowSoundDeleting;
    private bool _allowSoundEditing;
    private bool _showFavouritesOnly;

    [Obsolete("Parameterless constructor MainWindowViewModel() should not be used, it is only for design purposes")]
    public MainWindowViewModel() : this(new SoundDbContext()) { }

    public MainWindowViewModel(SoundDbContext dataContext)
    {
        Database = dataContext;

        AllSounds = new SourceList<SoundRowViewModel>();
        AllSounds.Edit(list =>
        {
            list.Clear();
            list.AddRange(Database.Sounds.Select(sound => new SoundRowViewModel(sound)));
        });
        Selection = new SelectionModel<SoundRowViewModel>
        {
            SingleSelect = false
        };

        Selection.SelectionChanged += (_, _) =>
        {
            AllowSoundEditing = Selection.Count == 1;
            AllowSoundDeleting = Selection.Count >= 1;
        };

        ShowYouTubeDialog = new Interaction<YouTubeDownloadDialogViewModel, Sound?>();
        ShowLocalDialog = new Interaction<string?, string[]?>();
        ShowEditDialog = new Interaction<EditSoundDialogViewModel, Sound?>();

        AddLocalSoundCommand = ReactiveCommand.Create(AddLocalSound);

        AddYouTubeSoundCommand = ReactiveCommand.CreateFromTask(AddYouTubeSound);

        EditSoundCommand = ReactiveCommand.Create(EditSound);

        DeleteSoundCommand = ReactiveCommand.CreateFromTask(DeleteSound);

        var filter = this.WhenAnyValue(model => model.ShowFavouritesOnly)
            .Select(BuildFilter);

        AllSounds.Connect()
            .Filter(filter)
            .ObserveOn(AvaloniaScheduler.Instance)
            .Bind(out _visibleSounds)
            .Subscribe();
    }

    public bool AllowSoundEditing
    {
        get => _allowSoundEditing;
        set => this.RaiseAndSetIfChanged(ref _allowSoundEditing, value);
    }

    public bool AllowSoundDeleting
    {
        get => _allowSoundDeleting;
        set => this.RaiseAndSetIfChanged(ref _allowSoundDeleting, value);
    }


    private SoundDbContext Database { get; }

    private SourceList<SoundRowViewModel> AllSounds { get; }

    public ReadOnlyObservableCollection<SoundRowViewModel> VisibleSounds => _visibleSounds;

    public SelectionModel<SoundRowViewModel> Selection { get; }

    private IReadOnlyList<SoundRowViewModel> SelectedItems => Selection.SelectedItems;

    public bool ShowFavouritesOnly
    {
        get => _showFavouritesOnly;
        set => this.RaiseAndSetIfChanged(ref _showFavouritesOnly, value);
    }

    public ICommand AddLocalSoundCommand { get; }

    public ICommand AddYouTubeSoundCommand { get; }

    public ICommand DeleteSoundCommand { get; }

    public ICommand EditSoundCommand { get; }

    public Interaction<YouTubeDownloadDialogViewModel, Sound?> ShowYouTubeDialog { get; }

    public Interaction<string?, string[]?> ShowLocalDialog { get; }

    public Interaction<EditSoundDialogViewModel, Sound?> ShowEditDialog { get; }

    private static IMsBoxWindow<ButtonResult> CreateConfirmationDialog()
    {
        return MessageBoxManager.GetMessageBoxStandardWindow(
            new MessageBoxStandardParams
            {
                ButtonDefinitions = ButtonEnum.YesNo,
                ContentTitle = "Confirm deletion",
                ContentHeader = "Please confirm",
                ContentMessage = "Are you sure you want to delete the selected sounds?",
                SizeToContent = SizeToContent.WidthAndHeight,
                ShowInCenter = true,
                MinWidth = 500,
                CanResize = true
            });
    }

    private Task AddYouTubeSound()
    {
        var dialogViewModel = new YouTubeDownloadDialogViewModel();

        ShowYouTubeDialog.Handle(dialogViewModel).Subscribe(newSound =>
        {
            if (newSound == null)
            {
                return;
            }

            Database.Add(newSound);
            Task.Run(() => Database.SaveChangesAsync());

            AllSounds.Add(new SoundRowViewModel(newSound));
        });

        return Task.CompletedTask;
    }

    private async Task DeleteSound()
    {
        var confirmDialog = CreateConfirmationDialog();
        var confirmDialogResult = await confirmDialog.Show();
        if (confirmDialogResult != ButtonResult.Yes)
        {
            return;
        }

        foreach (var selectedItem in SelectedItems.Reverse())
        {
            Database.Remove((object) selectedItem.Sound);
            await Database.SaveChangesAsync();

            AllSounds.Remove(selectedItem);
        }

        Selection.Clear();
    }

    private void EditSound()
    {
        if (SelectedItems.Count != 1)
        {
            return;
        }

        // var selectedItem = SelectedItems[0];
        var selectedItem = VisibleSounds[2];

        var dialogViewModel = new EditSoundDialogViewModel(selectedItem.Sound);

        ShowEditDialog
            .Handle(dialogViewModel)
            .Subscribe(editedSound =>
            {
                if (editedSound == null || selectedItem == null)
                {
                    return;
                }

                var soundRowViewModel = new SoundRowViewModel(editedSound);

                selectedItem.ClearHotKeyCommand.Execute(null);

                Database.Update(editedSound);
                Task.Run(() => Database.SaveChangesAsync());

                AllSounds.Edit(list =>
                {
                    // var index = list.IndexOf(selectedItem);
                    var index = 2;
                    list.Remove(selectedItem);
                    list.Insert(index, soundRowViewModel);
                });
            });
    }

    private void AddLocalSound()
    {
        ShowLocalDialog
            .Handle(null)
            .Subscribe(newPaths =>
            {
                if (newPaths == null)
                {
                    return;
                }

                foreach (var newPath in newPaths)
                {
                    var newSound = new Sound(newPath);

                    Database.Add(newSound);
                    Task.Run(() => Database.SaveChangesAsync());

                    AllSounds.Add(new SoundRowViewModel(newSound));
                }
            });
    }

    private static Func<SoundRowViewModel, bool> BuildFilter(bool showFavouritesOnly)
    {
        if (!showFavouritesOnly)
        {
            return _ => true;
        }

        return viewModel => viewModel.IsFavourite;
    }
}