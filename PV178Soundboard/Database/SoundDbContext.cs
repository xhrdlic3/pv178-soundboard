using System;
using System.IO;
using Avalonia.Input;
using Microsoft.EntityFrameworkCore;
using PV178Soundboard.Models;

namespace PV178Soundboard.Database;

public class SoundDbContext : DbContext
{
    public SoundDbContext()
    {
        const Environment.SpecialFolder folder = Environment.SpecialFolder.LocalApplicationData;
        var path = Environment.GetFolderPath(folder);
        DbPath = Path.Join(path, "soundboard.db");
    }

    public DbSet<Sound> Sounds => Set<Sound>();

    private string DbPath { get; }

    // The following configures EF to create a Sqlite database file in the
    // special "local" folder for your platform.
    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        options.UseSqlite($"Data Source={DbPath}");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .Entity<Sound>()
            .Property(sound => sound.KeyBind)
            .HasConversion(
                gesture => gesture == null ? null : gesture.ToString(),
                s => s == null ? null : KeyGesture.Parse(s)
            );
    }
}