using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using PV178Soundboard.Database;
using PV178Soundboard.ViewModels;
using PV178Soundboard.Views;

namespace PV178Soundboard;

public class App : Application
{
    public override void Initialize()
    {
        AvaloniaXamlLoader.Load(this);
    }

    public override void OnFrameworkInitializationCompleted()
    {
        if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
        {
            var db = new SoundDbContext();

            desktop.MainWindow = new MainWindow
            {
                DataContext = new MainWindowViewModel(db)
            };
        }

        base.OnFrameworkInitializationCompleted();
    }
}