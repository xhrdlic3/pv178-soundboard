using Avalonia.Input;
using Avalonia.Interactivity;

namespace PV178Soundboard.Models;

public class KeyBindArgs : RoutedEventArgs
{
    public KeyBindArgs(KeyGesture? keyGesture)
    {
        KeyGesture = keyGesture;
    }

    public KeyBindArgs() : this(null) { }

    public KeyGesture? KeyGesture { get; }
}