using System;
using Avalonia.Input;

namespace PV178Soundboard.Models;

public class Sound
{
    private string _path;

    public Sound(string path)
    {
        _path = path;
        Path = path;
        Name = System.IO.Path.GetFileName(path);
    }

    // Reason: Used by DB.
    // ReSharper disable once MemberCanBePrivate.Global
    // ReSharper disable once UnusedAutoPropertyAccessor.Global
    public int Id { get; set; }

    public string Path
    {
        get => _path;
        set
        {
            _path = value;
            Name = System.IO.Path.GetFileName(value);
        }
    }

    public string Name { get; private set; }
    public KeyGesture? KeyBind { get; set; }

    public bool IsFavourite { get; set; }

    private bool Equals(Sound other)
    {
        return Id == other.Id && _path == other._path;
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj))
        {
            return false;
        }

        if (ReferenceEquals(this, obj))
        {
            return true;
        }

        return obj.GetType() == GetType() && Equals((Sound) obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(_path, Id);
    }
}