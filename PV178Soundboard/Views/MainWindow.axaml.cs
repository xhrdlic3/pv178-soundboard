using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.ReactiveUI;
using PV178Soundboard.Models;
using PV178Soundboard.ViewModels;
using PV178Soundboard.ViewModels.Dialogs;
using PV178Soundboard.Views.Dialogs;
using ReactiveUI;

namespace PV178Soundboard.Views;

public partial class MainWindow : ReactiveWindow<MainWindowViewModel>
{
    public MainWindow()
    {
        InitializeComponent();

        this.WhenActivated(d => d(ViewModel!.ShowYouTubeDialog.RegisterHandler(ShowYouTubeDialogAsync)));
        this.WhenActivated(d => d(ViewModel!.ShowLocalDialog.RegisterHandler(ShowLocalDialogAsync)));
        this.WhenActivated(d => d(ViewModel!.ShowEditDialog.RegisterHandler(ShowEditDialogAsync)));
    }

    private async Task ShowEditDialogAsync(InteractionContext<EditSoundDialogViewModel, Sound?> interaction)
    {
        var dialog = new EditSoundDialog(interaction.Input);

        var result = await dialog.ShowDialog<Sound?>(this);

        interaction.SetOutput(result);
    }

    private async Task ShowYouTubeDialogAsync(
        InteractionContext<YouTubeDownloadDialogViewModel, Sound?> interaction)
    {
        var dialog = new YouTubeDownloadDialog
        {
            DataContext = interaction.Input
        };

        var result = await dialog.ShowDialog<Sound?>(this);
        interaction.SetOutput(result);
    }

    private async Task ShowLocalDialogAsync(InteractionContext<string?, string[]?> interaction)
    {
        await ShowLocalDialogAsync(interaction, this, true);
    }

    public static async Task ShowLocalDialogAsync(
        InteractionContext<string?, string[]?> interaction,
        Window parentWindow,
        bool allowMultipleSelection)
    {
        var dialog = new OpenFileDialog
        {
            AllowMultiple = allowMultipleSelection,
            Filters = { new FileDialogFilter { Name = "Sound", Extensions = { "mp3", "wav", "ogg" } } },
            Title = "Add Sound from path"
        };
        var maybeInput = interaction.Input;
        if (maybeInput != null)
        {
            dialog.Directory = maybeInput;
        }

        var paths = await dialog.ShowAsync(parentWindow);

        interaction.SetOutput(paths);
    }
}