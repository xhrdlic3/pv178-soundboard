using System;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.ReactiveUI;

namespace PV178Soundboard.Views.Dialogs;

public abstract class AbstractOwnerCenteredDialog<TViewModel> : ReactiveWindow<TViewModel> where TViewModel : class
{
    protected AbstractOwnerCenteredDialog()
    {
        WindowStartupLocation = WindowStartupLocation.CenterOwner;
    }

    public override async void Show()
    {
        base.Show();
        await Task.Delay(1);
        SetWindowStartupLocationWorkaround();
    }

    // protected abstract void Initialize();
    //
    // public T ViewModel => (T)DataContext!;

    /// <summary>
    ///     Fix center start position not working on Linux.
    ///     Source: https://github.com/AvaloniaUI/Avalonia/issues/6433
    /// </summary>
    private void SetWindowStartupLocationWorkaround()
    {
        if (OperatingSystem.IsWindows())
        {
            // Not needed for Windows, the bug is Linux only
            return;
        }

        var scale = PlatformImpl?.DesktopScaling ?? 1.0;

        var windowOwner = Owner?.PlatformImpl;
        if (windowOwner != null)
        {
            scale = windowOwner.DesktopScaling;
        }

        var windowRectangle = new PixelRect(PixelPoint.Origin,
            PixelSize.FromSize(ClientSize, scale));

        switch (WindowStartupLocation)
        {
            case WindowStartupLocation.CenterScreen:
            {
                var screen = Screens.ScreenFromPoint(windowOwner?.Position ?? Position);
                if (screen == null)
                {
                    return;
                }

                Position = screen.WorkingArea.CenterRect(windowRectangle).Position;
                break;
            }
            case WindowStartupLocation.CenterOwner:
            {
                if (windowOwner != null)
                {
                    Position = new PixelRect(windowOwner.Position,
                        PixelSize.FromSize(windowOwner.ClientSize, scale)).CenterRect(windowRectangle).Position;
                }

                break;
            }
            case WindowStartupLocation.Manual:
            default:
            {
                // Not considered for the functionality of this class
                break;
            }
        }
    }
}