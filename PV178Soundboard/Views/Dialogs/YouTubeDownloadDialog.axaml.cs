using System;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using PV178Soundboard.ViewModels.Dialogs;
using ReactiveUI;

namespace PV178Soundboard.Views.Dialogs;

public partial class YouTubeDownloadDialog : AbstractOwnerCenteredDialog<YouTubeDownloadDialogViewModel>
{
    public YouTubeDownloadDialog()
    {
        InitializeComponent();
        this.WhenActivated(d =>
        {
            d(ViewModel!.DownloadButtonClicked.Subscribe(model =>
            {
                if (model != null)
                {
                    Close(model);
                }
            }));
            d(ViewModel!.CancelButtonClicked.Subscribe(_ => Close(null)));
            d(ViewModel!.ShowBrowseDialog.RegisterHandler(ShowBrowseDialogAsync));
        });
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
        if (e.Key == Key.Escape)
        {
            e.Handled = true;
            Close(null);
        }

        base.OnKeyDown(e);
    }

    private async Task ShowBrowseDialogAsync(InteractionContext<string, string?> interaction)
    {
        var dialog = new OpenFileDialog
        {
            AllowMultiple = false,
            Filters = { new FileDialogFilter { Name = "Sound", Extensions = { "mp3", "wav", "ogg" } } },
            Directory = interaction.Input
        };

        var result = await dialog.ShowAsync(this);

        interaction.SetOutput(result is { Length: 1 } ? result[0] : null);
    }

    private void InitializeComponent()
    {
        AvaloniaXamlLoader.Load(this);
    }
}