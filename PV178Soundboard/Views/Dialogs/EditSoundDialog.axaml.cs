using System;
using Avalonia.Markup.Xaml;
using PV178Soundboard.Models;
using PV178Soundboard.ViewModels.Dialogs;
using ReactiveUI;

namespace PV178Soundboard.Views.Dialogs;

public partial class EditSoundDialog : AbstractOwnerCenteredDialog<EditSoundDialogViewModel>
{
    // reason: Avalonia requires a parameterless constructor for previews
    // ReSharper disable once UnusedMember.Global
    public EditSoundDialog() : this(new EditSoundDialogViewModel()) { }

    public EditSoundDialog(EditSoundDialogViewModel baseDataContext)
    {
        DataContext = baseDataContext;
        Vm = baseDataContext;
        InitializeComponent();

        this.WhenActivated(d =>
        {
            d(ViewModel!.OkButtonClicked.Subscribe(Close));
            d(ViewModel!.CancelButtonClicked.Subscribe(_ => Close(null)));
            d(ViewModel!.ShowBrowseDialog.RegisterHandler(context =>
                MainWindow.ShowLocalDialogAsync(context, this, false)));
        });
    }

    private EditSoundDialogViewModel Vm { get; }

    private void InitializeComponent()
    {
        AvaloniaXamlLoader.Load(this);
    }

    // reason: method must obey event handler signature
    // ReSharper disable once UnusedParameter.Local
    private void OnKeyBindChanged(object? sender, KeyBindArgs e)
    {
        Vm.KeyBind = e.KeyGesture;
    }

    // reason: method must obey event handler signature
    // ReSharper disable once UnusedParameter.Local
    private void SetInitialKeyBind(object? sender, EventArgs e)
    {
        KeyBindBox.SetKeyBind(Vm.KeyBind);
    }
}