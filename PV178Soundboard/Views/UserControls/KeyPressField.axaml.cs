using System;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Styling;
using PV178Soundboard.Models;
using PV178Soundboard.ViewModels.UserControls;

namespace PV178Soundboard.Views.UserControls;

public partial class KeyPressField : TextBox, IStyleable
{
    private static readonly RoutedEvent<KeyBindArgs> KeyBindChangedEvent =
        RoutedEvent.Register<KeyPressField, KeyBindArgs>(nameof(KeyBindChanged), RoutingStrategies.Bubble);

    private readonly KeyPressFieldViewModel _viewModel = new();

    public KeyPressField()
    {
        InitializeComponent();
        DataContext = _viewModel;
    }

    Type IStyleable.StyleKey => typeof(TextBox);

    public event EventHandler<KeyBindArgs> KeyBindChanged
    {
        add => AddHandler(KeyBindChangedEvent, value);
        remove => RemoveHandler(KeyBindChangedEvent, value);
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
        // Reason: not worth it
        // ReSharper disable once SwitchStatementHandlesSomeKnownEnumValuesWithDefault
        switch (e.Key)
        {
            case Key.Escape:
                Parent?.Focus();
                return;
            case Key.Delete:
                _viewModel.ClearKeyBind();

                e.Handled = true;
                RaiseEvent(new KeyBindArgs { RoutedEvent = KeyBindChangedEvent });

                Parent?.Focus();
                return;
            default:
                _viewModel.SetKeyBind(e);
                var keyBind = _viewModel.KeyBind;
                if (keyBind == null)
                {
                    Parent?.Focus();
                    return;
                }

                e.Handled = true;
                RaiseEvent(new KeyBindArgs(keyBind) { RoutedEvent = KeyBindChangedEvent });
                break;
        }
    }

    protected override void OnTextInput(TextInputEventArgs e)
    {
        // do not call base.OnTextInput(e), as that would duplicate text along with key bind
    }

    public void SetKeyBind(KeyGesture? keyBind)
    {
        _viewModel.SetKeyBind(keyBind);
    }
}