using System.Reactive;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.ReactiveUI;
using Avalonia.Threading;
using PV178Soundboard.ViewModels.UserControls;
using ReactiveUI;

namespace PV178Soundboard.Views.UserControls;

public partial class SoundRow : ReactiveUserControl<SoundRowViewModel>
{
    public SoundRow()
    {
        InitializeComponent();

        this.WhenActivated(d =>
        {
            d(ViewModel!.ChangeHotKey.RegisterHandler(ChangeKeyBind));
            ViewModel!.ChangeHotKeyCommand.Execute(null);
        });
    }

    private async Task ChangeKeyBind(InteractionContext<KeyGesture?, Unit> interaction)
    {
        var gesture = interaction.Input;

        if (gesture == null)
        {
            // Using UIThread.Invoke because of Call from invalid thread exception
            // SetValue is what HotKeyManager.SetHotKey does in the background anyways
            await Dispatcher.UIThread.InvokeAsync(() =>
                PlayButton.SetValue(HotKeyManager.HotKeyProperty, AvaloniaProperty.UnsetValue));
        }
        else
        {
            await Dispatcher.UIThread.InvokeAsync(() => HotKeyManager.SetHotKey(PlayButton, gesture));
        }
    }
}